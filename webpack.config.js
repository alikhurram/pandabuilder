var webpack = require('webpack');
var path = require('path');

var DIST_DIR = path.resolve(__dirname, "dist");
var SRC_DIR = path.resolve(__dirname, "src");

var config = {
    entry: SRC_DIR + "/app/index.js",
    output:{
        path: DIST_DIR + "/app",
        filename: "bundle.js",
        publicPath: "/app/"
    },
    module:{
        /*
        loaders: [
            {
                test: /\.js?/,
                include: SRC_DIR,
                exclude: /(node_modules|bower_components)/,
                loader: "babel-loader",
                query:{
                    presets: ['react','es2015','@babel/preset-env','stage-2']
                }
            }
    ]
    */
   
   rules: [
          {
            test: /\.js$/, // include .js files
            include: SRC_DIR,
            exclude: /node_modules/, // exclude any and all files in the node_modules folder
            use: [
              {
                loader: "babel-loader",
               
                query:{
                    presets: ['@babel/preset-react','@babel/preset-es2015','@babel/preset-env']
                }
              }
            ]
          }
        ]
   
   
    }
};

module.exports = config;